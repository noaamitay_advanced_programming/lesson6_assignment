﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Lesson6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        /*
            The function checks if the username and password is valit according to the file of users
        */
        private void button1_Click(object sender, EventArgs e)
        {
            String username = textBox1.Text;
            String password = textBox2.Text;
            bool check = true;

            string[] lines = System.IO.File.ReadAllLines("Users.txt"); //Open the file and insert the data to an array of strings

            for (int i = 0; i < lines.Length && check == true; i++) //Go through the array
            {
                if (lines[i].Split(',')[0] == username && lines[i].Split(',')[1] == password) //check if the username and password exist and match to each other
                {
                    check = false; //Stop the loop
                    this.Hide(); //Hide this form
                    Form2 f = new Form2(username); //Create the new form
                    f.ShowDialog(); //Open the new form
                    this.Close(); //Close this form
                }
            }

            if (check == true) //If there is no such user
            {
                MessageBox.Show("Error! The user doesn't exist / the password is wrong, Try again.");
            }
        }

        //Didn't erased because it's causing problems in the code
        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
