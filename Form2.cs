﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson6
{
    public partial class Form2 : Form
    {
        private String username;
        private string[] lines;

        //Tried to send the username from form1 to form2
        public Form2(String username)
        {
            InitializeComponent();
            this.username = username;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        // When form2 loads open the right file and insert all the data to an array of strings
        // (I had a problem with סעיף ג, which I don't know what caused it (can be in this function or in the next one)
        private void Form2_Load(object sender, EventArgs e)
        {
            String fileName = username + "BD.txt";
            lines = System.IO.File.ReadAllLines(fileName);
        }

        // The function suppose to check if there is a birthday in the specific date and if so to change the label
        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            bool check = true;

            //Save the date in the right order (MM/DD/YYYY)
            string day = (monthCalendar1.SelectionRange.Start.ToShortDateString()).Split('/')[0];
            string month = (monthCalendar1.SelectionRange.Start.ToShortDateString()).Split('/')[1];
            string year = (monthCalendar1.SelectionRange.Start.ToShortDateString()).Split('/')[2];
            string date = month + "/" + day + "/" + year;

            //Go though the file data and check if the date exists, if so - change the label to the name of the person who celebrates birthday in that date
            for (int i = 0; i < lines.Length && check == true; i++)
            {
                if (lines[i].Split(',')[1].Equals(date))
                {
                    label2.Text = "In the selected date: " + lines[i].Split(',')[0] + " is celebrating birthday!";
                    check = false; //Stop the loop
                }
            }
        }

        private void monthCalendar1_MouseCaptureChanged(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
        }
    }
}
